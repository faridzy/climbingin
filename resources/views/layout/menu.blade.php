<li>
    <a href="{{url('/backend')}}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span> </a>
</li>


<li>
    <a href="#"><i class="fa fa-table"></i> <span class="nav-label">Data Master</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li>
            <a href="{{url('/backend/master/users')}}">
                <i class="fa fa-circle-o"></i> Manajemen Pengguna
            </a>
        </li>
        <li>
            <a href="{{url('/backend/master/periods')}}">
                <i class="fa fa-circle-o"></i> Manajemen Periode
            </a>
        </li>
        <li>
            <a href="{{url('/backend/master/climbing-tool-categories')}}">
                <i class="fa fa-circle-o"></i> Manajemen Kategori Alat Pendaki</a>
        </li>

        <li>
            <a href="{{url('/backend/master/climbing-tools')}}">
                <i class="fa fa-circle-o"></i> Manajemen Alat Pendaki
            </a>
        </li>

    </ul>
</li>
<li>
    <a href="#"><i class="fa fa-balance-scale"></i> <span class="nav-label">Transaksi</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">

        <li>
            <a href="{{url('/backend/transaction/in')}}">
                <i class="fa fa-circle-o"></i> Transaksi Masuk
            </a>
        </li>
        <li>
            <a href="{{url('/backend/transaction/progress')}}">
                <i class="fa fa-circle-o"></i> Transaksi On Progress
            </a>
        </li>
        <li>
            <a href="{{url('/backend/transaction/record')}}">
                <i class="fa fa-circle-o"></i> Rekap Transaksi
            </a>
        </li>

    </ul>
</li>
<li>
    <a href="#"><i class="fa fa-feed"></i> <span class="nav-label">Feedback</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li>
            <a href="{{url('/backend/feedback')}}">
                <i class="fa fa-circle-o"></i> Feedback Masuk
            </a>
        </li>

    </ul>
</li>

<li>
    <a href="#"><i class="fa fa-line-chart"></i> <span class="nav-label">Laporan</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li>
            <a href="{{url('/backend/report/period-selling')}}">
                <i class="fa fa-circle-o"></i> Penyewaan Per Periode
            </a>
        </li>
        <li>
            <a href="{{url('/backend/report/price-statistic')}}">
                <i class="fa fa-circle-o"></i> Statistik Harga Sewa Alat
            </a>
        </li>

    </ul>
</li>

<li>
    <a href="{{url('/logout')}}"><i class="fa fa-sign-out"></i> <span class="nav-label">Logout</span></a>
</li>