<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Nama Alat Pendaki </label>
        <div class='col-sm-7 col-xs-12'>
            <input type="text" name="climbing_tool_name" class="form-control" value="{{$data->climbing_tool_name}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Deskripsi </label>
        <div class='col-sm-9 col-xs-12'>
            <input type="text" name="climbing_tool_description" class="form-control" value="{{$data->climbing_tool_description}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Harga Sewa </label>
        <div class='col-sm-5 col-xs-12'>
            <input type="number" name="climbing_tool_price" class="form-control" value="{{$data->climbing_tool_price}}" required="">
        </div>
    </div>
    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Stok </label>
        <div class='col-sm-5 col-xs-12'>
            <input type="number" name="climbing_tool_stock" class="form-control" value="{{$data->climbing_tool_stock}}" required="">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Gambar</label>
        <div class="col-sm-10">
            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                <div class="form-control" data-trigger="fileinput">
                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                    <span class="fileinput-filename"></span>
                </div>
                <span class="input-group-addon btn btn-default btn-file">
                    <span class="fileinput-new">Select file</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="climbing_tool_picture" accept="image/*,application/pdf,">
                </span>
                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
            </div>
        </div>
    </div>

    <div class='form-group'>
        <label for='nama' class='col-sm-2 col-xs-12 control-label'>Kategori Alat Pendaki</label>
        <div class='col-sm-8 col-xs-12'>
            <select name="climbing_tool_categories_id" class="form-control">
                <option disabled>Pilih Data</option>
                @foreach($categoryOption as $num => $item)
                    @if(isset($data->climbing_tool_categories_id))
                        @if($data->climbing_tool_categories_id == $item->id)
                            <option value="{{$item->id}}" selected="selected">{{$item->climbing_tool_category_name}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->climbing_tool_category_name}}</option>
                        @endif
                    @else
                        <option value="{{$item->id}}">{{$item->climbing_tool_category_name}}</option>
                    @endif

                @endforeach
            </select>
        </div>
    </div>

    <div class='form-group'>
        <label class='col-sm-2 col-xs-12 control-label'></label>
        <div class='col-sm-9 col-xs-12'>
            <input type="submit" class="btn btn-primary" value="Simpan Data">
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id}}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/backend/master/climbing-tools/save', data, '#result-form-konten');
        })
    })

</script>
