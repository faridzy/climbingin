<table class="table table-striped table-bordered table-hover">
    <tr>
        <td width="25%">Nama</td>
        <td width="3%">:</td>
        <th>{{$order->getUser->user_fullname}}</th>
    </tr>

    <tr>
        <td>Tanggal Order</td>
        <td>:</td>
        <th>{{date("d-M-Y H:i:s", strtotime($order->order_created_at))}}</th>
    </tr>

    <tr>
        <td>Total Belanja</td>
        <td>:</td>
        <th>Rp. {{number_format($order->order_total)}}</th>
    </tr>

    <tr>
        <td>Biaya Pengiriman</td>
        <td>:</td>
        <th>Rp. {{number_format($order->order_shipping_cost)}}</th>
    </tr>

    <tr>
        <td>Bukti Transfer</td>
        <td>:</td>
        <td>
            @if(is_null($order->order_pay_receipt) || $order->order_is_pay == 0)
                <div class="alert alert-danger">Belum upload</div>
            @else
                <img src="{{asset('public/uploads/receipt')}}/{{$order->order_pay_receipt}}" width="100%">
            @endif
        </td>
    </tr>
    <tr>
        <td>Status</td>
        <td>:</td>
        <th>
            {{$order->getOrderStatus->order_status_name}}
        </th>
    </tr>

</table>


<br>
<h3>Detail Belanja</h3>
<br>

<table class="table table-striped table-bordered table-hover" id="table-detail">
    <thead>
    <tr>
        <th>No</th>
        <th>Nama Alat Pendaki</th>
        <th>Harga</th>
        <th>Stok</th>
        <th>Kuantitas</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orderItems as $num => $item)
        <tr>
            <td>{{$num+1}}</td>
            <td>{{$item->getClimbingTool->climbing_tool_name}}</td>
            <td>Rp. {{number_format($item->getClimbingTool->climbing_tool_price)}}</td>
            <td>{{$item->getClimbingTool->climbing_tool_stock}}</td>
            <td>{{$item->order_item_quantity}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<style>
    th {
        text-align: left;
    }
</style>


<script>
    $("#table-detail").dataTable();
</script>