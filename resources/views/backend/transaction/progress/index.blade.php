@extends('layout.main')
@section('Title', 'Manajemen Transaksi Progress')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Transaksi Progrress</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Transaksi</a>
                </li>


                <li class="active">
                    <a>Transaksi Progress</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                    <table class="table table-striped table-bordered table-hover" id="table-data">
                        <thead>
                        <tr>
                            <th width="3%">No</th>
                            <th>Nama Pembeli</th>
                            <th>Waktu</th>
                            <th>Total</th>
                            <th>Biaya Pengiriman</th>
                            <th>Jenis Pengiriman</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $num => $item)
                            <tr>
                                <td>{{$num+1}}</td>
                                <td>{{$item->getUser->user_fullname}}</td>
                                <td>{{date("d-M-Y H:i:s", strtotime($item->order_created_at))}}</td>
                                <td>Rp. {{number_format($item->order_total)}}</td>
                                <td>Rp. {{number_format($item->order_shipping_cost)}}</td>
                                <td width="10%">JNE - {{$item->service_code}}</td>
                                <td width="10%" align="center">
                                    <a onclick="loadModal(this)" target="/backend/transaction/progress/detail" data="id={{$item->id}}"
                                       class="btn btn-primary btn-xs glyphicon glyphicon-briefcase" title="Detail Data"></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>


                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    </div>
    <br />

    <div class="row">
        &nbsp;
    </div>

@endsection


@section('scripts')
    <script>
        $("#table-data").dataTable();
    </script>
@endsection