@extends('layout.main')
@section('Title', 'Rekap Feedback')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Feedback</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Feedback</a>
                </li>


                <li class="active">
                    <a>Detail</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                    <table class="table table-striped table-bordered table-hover" id="table-data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Aktor</th>
                            <th>Tanggal Transaksi</th>
                            <th>Total Pembayaran</th>
                            <th>Feedback</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $num => $item)
                            <tr>
                                <td>{{$num+1}}</td>
                                <td>{{$item->getUser->user_fullname}}</td>
                                <td>{{date("d-M-Y H:i:s", strtotime($item->order_created_at))}}</td>
                                <td>Rp. {{number_format($item->order_total + $item->order_shipping_cost)}}</td>
                                <td>
                                    @if($item->order_user_feedback == 1)
                                        <span class="form-control btn-success" align="center">
                                            Puas
                                        </span>
                                    @else
                                        <span class="form-control btn-danger" align="center">
                                            Tidak puas
                                        </span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection

@section('scripts')
    <script>
        $("#table-data").dataTable();
    </script>
@endsection