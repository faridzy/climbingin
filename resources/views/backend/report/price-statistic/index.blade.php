@extends('layout.main')
@section('Title', 'Laporan Penjualan Per Periode')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Report</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">Home</a>
                </li>

                <li>
                    <a href="#">Report</a>
                </li>


                <li class="active">
                    <a>Statistik Harga</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="row">
                            <div class="chart">
                                <!-- Sales Chart Canvas -->
                                <canvas id="salesChart" style="height: auto;"></canvas>
                            </div>
                        </div>

                    </div>

                    <br><br>
                    <table class="table table-striped table-bordered table-hover" id="table-data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Alat Pendaki</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $num => $item)
                            <tr>
                                <td>{{$num+1}}</td>
                                <td>{{$item['name']}}</td>
                                <td>Rp. {{number_format($item['price'])}}</td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>


                    <div class="clearfix"></div>
                </div>

            </div>
        </div>

    </div>
    <br />

    <div class="row">
        &nbsp;
    </div>
@section('scripts')
    <script src="{{asset('public/template/plugins/chartjs/Chart.min.js')}}"></script>
    <script>
        $(function () {
            'use strict';

            var max = 0;
            var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
            var salesChart = new Chart(salesChartCanvas);
            var normalPriceArray = [];
            var spesificPriceArray = [];
            var labelArray = [];
                    @foreach($data as $num => $item)
            var tmp ={{$item['price']}};
            if(tmp > max )
            {
                max = tmp;
            }


            normalPriceArray.push({{$item['price']}});

            labelArray.push('{{$item['name']}}');
                    @endforeach

            var salesChartData = {
                    labels: labelArray,
                    datasets: [
                        {
                            label: "Harga",
                            fillColor: "rgba(60,141,188,0.9)",
                            strokeColor: "rgba(60,141,188,0.8)",
                            pointColor: "#3b8bba",
                            pointStrokeColor: "rgba(60,141,188,1)",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(60,141,188,1)",
                            data: normalPriceArray
                        },
                    ]
                };

            var salesChartOptions = {
                showScale: true,
                scaleOverride : true,
                scaleStepWidth : max/2,
                scaleSteps : 3,
                scaleShowGridLines: false,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                scaleShowHorizontalLines: true,
                scaleShowVerticalLines: true,
                bezierCurve: true,
                bezierCurveTension: 0.3,
                pointDot: true,
                pointDotRadius: 5,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
        responsive: true
      };

      salesChart.Line(salesChartData, salesChartOptions);
     });


     $("#table-data").dataTable();
</script>

@endsection
@endsection


