<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'Api\ApiLoginController@login');
Route::post('/register', 'Api\ApiRegisterController@register');
Route::get('/climbing-tool-category', 'Api\ApiClimbingToolCategoryController@index');
Route::get('/climbing-tool', 'Api\ApiClimbingToolController@index');
Route::get('/{id}/profile', 'Api\ApiProfileController@index');
Route::post('/update-profile', 'Api\ApiProfileController@updateProfile');

Route::group(['prefix' => 'cart', 'namespace' => 'Api\Cart'], function () {
    Route::get('/', 'ApiCartController@index');
    Route::get('/{user_id}/list', 'ApiCartController@listProductInCart');
    Route::post('/add-to-cart', 'ApiCartController@addToCart');
    Route::post('/add-quantity', 'ApiCartController@addQuantity');
    Route::post('/reduce-quantity', 'ApiCartController@reduceQuantity');
    Route::get('/{user_id}/normalization-cart', 'ApiCartController@normalizationCart');
});

Route::get('/climbing-tool/recomendation', 'Api\ApiClimbingToolController@getRecomendation');

Route::group(['prefix' => 'history', 'namespace' => 'Api'], function () {
    Route::get('/{user_id}/all', 'ApiHistoryTransaksiController@all');
    Route::get('/{user_id}/on-going', 'ApiHistoryTransaksiController@onGoingTransaction');
    Route::get('/{user_id}/success', 'ApiHistoryTransaksiController@onSuccessTransaction');
});


Route::group(['prefix' => 'payment', 'namespace' => 'Api'], function () {
    Route::post('/checkout', 'ApiPaymentController@checkOut');
    Route::post('/upload-receipt', 'ApiPaymentController@uploadReceipt');
});


Route::group(['prefix' => 'feedback', 'namespace' => 'Api'], function () {
    Route::post('/save', 'ApiFeedbackController@feedback');
});

Route::post('/search', 'Api\ApiIkanController@search');
Route::group(['prefix' => 'raja-ongkir', 'namespace' => 'Api\RajaOngkir'], function () {
    Route::get('/province', 'RajaOngkirController@getProvince');
    Route::get('/city', 'RajaOngkirController@getCity');
    Route::post('/cost', 'RajaOngkirController@getCost');
});