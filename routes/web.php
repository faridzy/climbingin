<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Backend\BackendController@index')->middleware('login-verification');
Route::get('login', 'LoginController@index');
Route::get('logout', 'LoginController@logout');
Route::post('validate-login', 'LoginController@login');
Route::group(['prefix'=>'backend','namespace'=>'Backend','middleware'=>'login-verification'],function (){
    Route::get('/', 'BackendController@index');
    Route::group(['prefix' => 'master', 'namespace' => 'Master', 'middleware' => 'login-verification'], function () {
        Route::group(['prefix' => 'periods'], function () {
            Route::get('/', 'PeriodController@index');
            Route::post('/form', 'PeriodController@form');
            Route::post('/save', 'PeriodController@save');
            Route::post('/delete', 'PeriodController@delete');
        });

        Route::group(['prefix' => 'climbing-tool-categories'], function () {
            Route::get('/', 'ClimbingToolCategoryController@index');
            Route::post('/form', 'ClimbingToolCategoryController@form');
            Route::post('/save', 'ClimbingToolCategoryController@save');
            Route::post('/delete', 'ClimbingToolCategoryController@delete');
        });

        Route::group(['prefix' => 'climbing-tools'], function () {
            Route::get('/', 'ClimbingToolController@index');
            Route::post('/form', 'ClimbingToolController@form');
            Route::post('/save', 'ClimbingToolController@save');
            Route::post('/delete', 'ClimbingToolController@delete');
        });

        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'UserController@index');
            Route::post('/form', 'UserController@form');
            Route::post('/save', 'UserController@save');
            Route::post('/delete', 'UserController@delete');
        });

    });

    Route::group(['prefix' => 'transaction', 'namespace' => 'Transaction'], function () {
        Route::group(['prefix' => 'in', 'middleware' => 'login-verification'], function () {
            Route::get('/', 'InTransactionController@index');
            Route::post('/detail', 'InTransactionController@detail');
            Route::post('/update-to-process', 'InTransactionController@updateToProcess');
        });

        Route::group(['prefix' => 'progress', 'middleware' => 'login-verification'], function () {
            Route::get('/', 'ProgressTransactionController@index');
            Route::post('/detail', 'ProgressTransactionController@detail');
            Route::post('/update-status', 'ProgressTransactionController@updateStatus');
        });

        Route::group(['prefix' => 'record', 'middleware' => 'login-verification'], function () {
            Route::get('/', 'RecordTransactionController@index');
            Route::post('/show', 'RecordTransactionController@show');
            Route::post('/detail', 'RecordTransactionController@detail');
        });
    });


    Route::group(['prefix' => 'feedback', 'namespace' => 'Feedback', 'middleware' => 'login-verification'], function () {
        Route::get('/', 'FeedbackController@index');
        Route::get('/in', 'FeedbackController@in');
    });


    Route::group(['prefix' => 'report', 'namespace' => 'Report'], function () {
        Route::group(['prefix' => 'stock', 'middleware' => 'login-verification'], function () {
            Route::get('/', 'StockController@index');
            Route::post('/show', 'StockController@show');
        });

        Route::group(['prefix' => 'period-selling', 'middleware' => 'login-verification'], function () {
            Route::get('/', 'PeriodSellingController@index');
            Route::post('/show', 'PeriodSellingController@show');
        });

        Route::group(['prefix' => 'price-statistic', 'middleware' => 'login-verification'], function () {
            Route::get('/', 'PriceStatisticController@index');
            Route::post('/show', 'PriceStatisticController@show');
        });
    });
});