<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.49
 */

namespace App\Http\Controllers\Api;


use App\Classes\MessageSystemFunctionalClass;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ApiLoginController extends Controller
{

    private $messageSystemFunctionalClass;

    public  function __construct(){

        $this->messageSystemFunctionalClass = new MessageSystemFunctionalClass();

    }

    public function login(Request $request){
        $apiName='VALIDATE_LOGIN';
        $username=$request->username;
        $password=sha1($request->password);

        $sendingParams = [
            'username' => $username,
            'password' => $password
        ];

        if(is_null($username)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName,404,'Missing required parameter username!',json_encode($sendingParams));
        }

        $activeUser=User::where(['user_username'=>$username,'user_role'=>2])->first();
        if(is_null($activeUser)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Username not found!", json_encode($sendingParams) );
        }

        if($activeUser->user_password != $password){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 401, "Password not match!", json_encode($sendingParams) );
        }

        $activeUser->user_last_login = date("Y-m-d H:i:s");
        $activeUser->save();

        if(empty($activeUser->user_picture)){
            $userPicture='public/assets/img/user.jpg';
        }else{
            $userPicture='public/uploads/users/'.$activeUser->user_picture;
        }
        $data = [
            'id' => $activeUser->id,
            'username' => $activeUser->user_username,
            'identity_number' => $activeUser->user_identity_number,
            'address' => $activeUser->user_address,
            'post_code' => $activeUser->user_post_code,
            'picture' =>$userPicture,
            'city' => $activeUser->getCity->city_name,
            'phone_number' => $activeUser->user_phone_number,
            'fullname' => $activeUser->user_fullname,
            'email' => $activeUser->user_email,
            'gender'=>$activeUser->gender
        ];

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Login Success!',
            'data' => $data
        ];


        return response()->json($params);

    }

}