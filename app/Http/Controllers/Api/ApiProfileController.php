<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.51
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Classes\MessageSystemFunctionalClass;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ApiProfileController extends Controller
{
    private $messageSystemFunctionalClass;

    public  function __construct(){

        $this->messageSystemFunctionalClass = new MessageSystemFunctionalClass();

    }

    public function index($id)
    {
        $activeUser = User::find($id);

        if(is_null($activeUser)){
            $params = [
                'code' => 404,
                'description' => 'Not Found',
                'message' => 'Get profile Failed!',
                'data' => []
            ];

        }else{

            $data = [
                'id' => $activeUser->id,
                'username' => $activeUser->user_username,
                'identity_number' => $activeUser->user_identity_number,
                'address' => $activeUser->user_address,
                'post_code' => $activeUser->user_post_code,
                'picture' => 'public/uploads/users/'.$activeUser->user_picture,
                'city' => $activeUser->getCity->city_name,
                'phone_number' => $activeUser->user_phone_number,
                'name' => $activeUser->user_fullname,
                'email' => $activeUser->user_email,
                'birthdate'=>$activeUser->user_birthdate,
                'gender'=>$activeUser->user_gender
            ];

            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Get profile Success!',
                'data' => $data
            ];

        }

        return response()->json($params);

    }

    public function updateProfile(Request $request){

        $apiName = "UPDATE_PROFILE";
        $username = $request->username;
        $password = $request->password;
        $confirmPassword = $request->confirm_password;
        $identityNumber = $request->identity_number;
        $address = $request->address;
        $postCode = $request->post_code;
        $phoneNumber = $request->phone_number;
        $name = $request->name;
        $email = $request->email;
        $id = $request->id;

        $sendingParams = [

            'username' => $username,
            'password' => $password,
            'confirm_password'=>$confirmPassword,
            'identity_number' => $identityNumber,
            'address' => $address,
            'phone_number' => $phoneNumber,
            'name' => $name,
            'email' => $email,
            'postcode' => $postCode,
            'id'=>$id

        ];

        if(is_null($id)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter id!", json_encode($sendingParams) );
        }

        if(is_null($username)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter username!", json_encode($sendingParams) );
        }

        if(is_null($password)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter password!", json_encode($sendingParams) );
        }

        if(is_null($confirmPassword)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter confirm_password!", json_encode($sendingParams) );
        }

        if(is_null($identityNumber)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter identity_number!", json_encode($sendingParams) );
        }

        if(is_null($address)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter address!", json_encode($sendingParams) );
        }

        if(is_null($postCode)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter postcode!", json_encode($sendingParams) );
        }

        if(is_null($phoneNumber)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter phone_number!", json_encode($sendingParams) );
        }

        if(is_null($name)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter name!", json_encode($sendingParams) );
        }

        if(is_null($email)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter email!", json_encode($sendingParams) );
        }


        $data = User::find($id);
        if(is_null($data))
        {
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'User not found!', json_encode($sendingParams));
        }

        if($password != $confirmPassword)
        {
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'New password not match with confirmation password!', json_encode($sendingParams));
        }


        $data->user_password = sha1($password);
        $data->user_address = $address;
        $data->user_post_code = $postCode;
        $data->user_phone_number = $phoneNumber;
        $data->user_fullname = $name;
        $data->user_email = $email;

        $picture = $request->file('user_picture');
        if (isset($picture)) {
            $destinationPath='public/uploads/users';
            if (!file_exists($destinationPath)) {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            $filename = date("YmdHis"). '-' . $picture->getClientOriginalName();
            if ($picture->move($destinationPath, $filename)) {
                $data->user_picture = $filename;
            }
        }

        try{
            $data->save();

            $activeUser = User::find($id);
            $data = [
                'id' => $activeUser->id,
                'username' => $activeUser->user_username,
                'identity_number' => $activeUser->user_identity_number,
                'address' => $activeUser->user_address,
                'post_code' => $activeUser->user_post_code,
                'picture' => 'public/uploads/users/'.$activeUser->user_picture,
                'city' => $activeUser->getCity->city_name,
                'phone_number' => $activeUser->user_phone_number,
                'name' => $activeUser->user_fullname,
                'email' => $activeUser->user_email,
                'birthdate'=>$activeUser->user_birthdate,
                'gender'=>$activeUser->user_gender
            ];

            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Update profile success!',
                'data' => $data
            ];

            return response()->json($params);

        }catch(Exception $e){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'Failed to update user profile!', json_encode($sendingParams));
        }

    }


}