<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.54
 */

namespace App\Http\Controllers\Api;
use App\Classes\MessageSystemFunctionalClass;
use App\Models\ClimbingToolCategory;


class ApiClimbingToolCategoryController
{
    private $messageSystemFunctionalClass;

    public  function __construct(){

        $this->messageSystemFunctionalClass = new MessageSystemFunctionalClass();

    }

    public function index(){
        $data = ClimbingToolCategory::orderBy('climbing_tool_category_name', 'ASC')->get();

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get climbing tool category Success!',
            'data' => $data
        ];

        return response()->json($params);
    }


}