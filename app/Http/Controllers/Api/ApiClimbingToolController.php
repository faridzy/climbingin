<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.54
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Classes\MessageSystemFunctionalClass;
use App\Models\ClimbingTool;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;

class ApiClimbingToolController extends Controller
{
    private $messageSystemFunctionalClass;

    public  function __construct(){

        $this->messageSystemFunctionalClass = new MessageSystemFunctionalClass();

    }

    public function index(){
        $data = ClimbingTool::orderBy('climbing_tool_name', 'ASC')->get();
        foreach ($data as $item){
            $item->climbing_tool_picture='public/uploads/climbing-tools/'.$item->climbing_tool_categories_id.'/'.$item->climbing_tool_picture;
        }
        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get climbing tool Success!',
            'data' => $data
        ];

        return response()->json($params);
    }

    public function getRecommendation(){
        $successOrders = Order::where(['order_order_status_id' => 1])->limit(5)->orderBy('id', 'DESC')->get();
        $arrayClimbingToolId = [];
        foreach($successOrders as $num => $item)
        {
            $orderItems = $item->getOrderItems;
            foreach ($orderItems as $key => $orderItem){
                if(!isset($arrayClimbingToolId[$orderItem->order_climbing_tools_id])){
                    $arrayClimbingToolId[$orderItem->order_climbing_tools_id] = $orderItem->order_climbing_tools_id;
                }
            }
        }

        $dataClimbingTool = ClimbingTool::where('climbing_tool_stock', '>', 0)
            ->whereIn('id', $arrayClimbingToolId)
            ->get();

        $data = [];
        foreach ($dataClimbingTool as $num => $item){
            if ($item->climbing_tool_stock > 0){
                $data[] = [
                    'id' => $item->id,
                    'climbing_tool_name' => $item->climbing_tool_name,
                    'climbing_tool_category_name' => $item->getCategory->climbing_tool_category_name,
                    'climbing_tool_price'=>$item->climbing_tool_price,
                    'climbing_tool_stock'=>$item->climbing_tool_stock,
                    'climbing_tool_picture'=>'public/uploads/climbing-tools'.$item->climbing_tool_categories_id.'/'.$item->climbing_tool_picture,
                ];
            }
        }
        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get climbing tool recommendation Success!',
            'data' => $data
        ];


        return response()->json($params);
    }

    public function filtered(Request $request){
        $category = $request->get('category');
        $price=$request->get('price');
        $data=[];

        if(!is_null($category) && !is_null($price)) {
            $dataClimbingTool = ClimbingTool::where(['climbing_tool_categories_id' => $category])
                ->orderBy('climbing_tool_price',$price)
                ->get();

            foreach ($dataClimbingTool as $num => $item) {
                if ($item->climbing_tool_stock > 0){
                    $data[] = [
                        'id' => $item->id,
                        'climbing_tool_name' => $item->climbing_tool_name,
                        'climbing_tool_category_name' => $item->getCategory->climbing_tool_category_name,
                        'climbing_tool_price'=>$item->climbing_tool_price,
                        'climbing_tool_stock'=>$item->climbing_tool_stock,
                        'climbing_tool_picture'=>'public/uploads/climbing-tools'.$item->climbing_tool_categories_id.'/'.$item->climbing_tool_picture,
                    ];
                }


            }
        }
        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get filtered climbing tool Success!',
            'data' => $data
        ];


        return response()->json($params);
    }

    public function search(Request $request)
    {
        $apiName = "SEARCH";
        $query = $request->input('query');
        $sendingParams = [
            'query' => $query
        ];

        if(is_null($query)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter query!", json_encode($sendingParams) );
        }

        $dataClimbingTool = ClimbingTool::whereRaw("climbing_tool_name like  '%".$query."%' ")
            ->get();

        $data = [];
        foreach ($dataClimbingTool as $num => $item) {
            if ($item->climbing_tool_stock > 0){
                $data[] = [
                    'id' => $item->id,
                    'climbing_tool_name' => $item->climbing_tool_name,
                    'climbing_tool_category_name' => $item->getCategory->climbing_tool_category_name,
                    'climbing_tool_price'=>$item->climbing_tool_price,
                    'climbing_tool_stock'=>$item->climbing_tool_stock,
                    'climbing_tool_picture'=>'public/uploads/climbing-tools'.$item->climbing_tool_categories_id.'/'.$item->climbing_tool_picture,
                ];
            }


        }

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Get climbing tool Success!',
            'data' => $data
        ];


        return response()->json($params);


    }



}