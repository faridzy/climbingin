<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.53
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Classes\MessageSystemFunctionalClass;
use Illuminate\Http\Request;
use App\Models\Order;

class ApiFeedbackController extends Controller
{
    private $messageSystemFunctionalClass;

    public  function __construct(){

        $this->messageSystemFunctionalClass = new MessageSystemFunctionalClass();

    }

    public  function feedback(Request $request){
        $apiName='FEEDBACK_USER';
        $orderId = $request->input('order_id');
        $feedbackValue = $request->input('feedback_value');

        $sendingParams = [
            'order_id' => $orderId,
            'feedback_value' => $feedbackValue
        ];

        if(is_null($orderId)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter order_id!", json_encode($sendingParams) );
        }

        if(is_null($feedbackValue)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter feedback_value!", json_encode($sendingParams) );
        }

        $order = Order::find($orderId);
        if(is_null($order)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Order not found!", json_encode($sendingParams) );
        }

        try{
            $order->order_user_feedback = $feedbackValue;
            $order->save();

            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Feedback successfully save!',
            ];
            return response()->json($params);

        }catch(\Exception $e){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 500, "Failed to store feedback value!", json_encode($sendingParams));
        }

    }



}