<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.54
 */

namespace App\Http\Controllers\Api\Cart;
use App\Classes\MessageSystemFunctionalClass;
use App\Http\Controllers\Controller;
use App\Models\ClimbingTool;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderItemLog;
use App\Models\OrderStatus;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Period;

class ApiCartController extends Controller
{
    private $messageSystemFunctionalClass;

    public  function __construct(){

        $this->messageSystemFunctionalClass = new MessageSystemFunctionalClass();

    }

    public function addToCart(Request $request)
    {
        $apiName = "ADD_TO_CART";
        $userId = $request->input('user_id');
        $currentYear = $request->input('current_year');
        $requestParams = [
            'user_id' => $userId,
            'current_year' => $currentYear,
        ];

        if(is_null($userId)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Missing required paramter user_id', json_encode($requestParams) );
        }

        if(is_null($currentYear)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Missing required paramter current_year', json_encode($requestParams) );
        }

        $currentPeriod = Period::where(['period_name' => $currentYear])->first();
        if(is_null($currentPeriod)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'Period not found!', json_encode($requestParams) );
        }

        $currentUser = User::find($userId);
        if(is_null($currentUser)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'User not found!', json_encode($requestParams) );
        }



        $currentOrder = Order::where(['order_users_id' => $userId, 'order_order_status_id' => 1])->first();
        if(is_null($currentOrder)){
            $currentOrder = new Order();
            $currentOrder->order_users_id = $userId;
            $currentOrder->order_periods_id = $currentPeriod->id;
            $currentOrder->order_created_at = date("Y-m-d H:i:s");
            $currentOrder->order_total = 0;
            $currentOrder->order_shipping_cost = 0;
            $currentOrder->order_order_status_id = 1; //order baru / berjalan
            $currentOrder->order_payment_type_id = 1;
            try{
                $currentOrder->save();
            }catch (Exception $e){
                return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 500, 'Order could not created!', json_encode($requestParams));
            }
        }



        /**
         * @proses add to cart
         **/
        $climbingToolId = $request->input('order_climbing_tools_id');
        if(is_null($climbingToolId)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Missing required paramter order_climbing_tools_id', json_encode($requestParams) );
        }

        $requestParams["order_climbing_tools_id"] = $climbingToolId;

        $climbingToolItem = ClimbingTool::find($climbingToolId);
        if(is_null($climbingToolItem)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'Climbing tool item not found!', json_encode($requestParams) );
        }

        $orderItem = OrderItem::where(['order_item_orders_id' => $currentOrder->id, 'order_item_climbing_tools_id' => $climbingToolId])->first();
        if(is_null($orderItem)){

            if(($climbingToolItem->climbing_tool_stock - 1) < 0)
            {
                return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'CLimbing tool item out of stock!', json_encode($requestParams) );
            }

            $orderItem = new OrderItem();
            $orderItem->order_item_quantity = 1;
            $orderItem->order_item_climbing_tools_id = $climbingToolId;
            $orderItem->order_item_orders_id = $currentOrder->id;

            try{
                $orderItem->save();

            }catch (\Exception $e){
                return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 500, 'Order item couldnot created!', json_encode($requestParams));
            }

        }else{
            $orderItem->order_item_quantity += 1;

            if(($climbingToolItem->climbing_tool_stock - $orderItem->order_item_quantity) < 0)
            {
                return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'Climbing tool item out of stock!', json_encode($requestParams) );
            }

            try{
                $orderItem->save();
            }catch (\Exception $e){
                return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 500, 'Order item couldnot added!', json_encode($requestParams));
            }
        }

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Success adding Climbing tool item to cart!',
        ];


        return response()->json($params);
    }

    public function listProductInCart(Request $request, $user_id)
    {
        $apiName = "LIST_PRODUCT_IN_CART";
        $requestParams = [
            'user_id' => $user_id
        ];
        $activeUser = User::find($user_id);
        if(is_null($activeUser)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'User not found!', json_encode($requestParams));
        }

        $orders = Order::where(['order_users_id' => $user_id, 'order_order_status_id' => 1])->get();
        $data = [];
        foreach ($orders as $num => $item)
        {
            foreach ($item->getOrderItems as $no => $orderItem){

                $data[] = [
                    'order_item_id' => $orderItem->id,
                    'climbing_tool_name' => $orderItem->getClimbingTool->climbing_tool_name,
                    'quantity' => $orderItem->order_item_quantity,
                    'price' => $orderItem->getClimbingTool->climbing_tool_price,
                    'picture' => 'public/uploads/climbing-tools/'.$orderItem->getClimbingTool->climbing_tool_categories_id.'/'.$orderItem->getClimbingTool->climbing_tool_picture,
                ];
            }
        }

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Success get product in cart!',
            'data' => $data
        ];


        return response()->json($params);

    }

    public function addQuantity(Request $request)
    {
        $apiName = "API_ADD_QUANTITY";
        $userId = $request->input('user_id');
        $orderItemId = $request->input('order_item_id');
        $requestParams = [
            'user_id' => $userId,
            'order_item_id' => $orderItemId,
        ];

        if(is_null($userId)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Missing required paramter user_id', json_encode($requestParams) );
        }

        if(is_null($orderItemId)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Missing required paramter order_item_id', json_encode($requestParams) );
        }

        $activeUser = User::find($userId);
        if(is_null($activeUser)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'User not found!', json_encode($requestParams) );
        }

        $orderItem = OrderItem::find($orderItemId);
        if(is_null($orderItem)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'Order item not found!', json_encode($requestParams) );
        }

        $climbingToolItem = ClimbingTool::find($orderItem->order_item_climbing_tools_id);
        if(is_null($climbingToolItem)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'Climbing tool item not found!', json_encode($requestParams) );
        }

        try{
            $orderItem->order_item_quantity += 1;

            if(($climbingToolItem->climbing_tool_stock - $orderItem->order_item_quantity) < 0)
            {
                return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'Climbing tool item out of stock!', json_encode($requestParams) );
            }

            $orderItem->save();

            $orders = Order::where(['order_users_id' => $userId])->get();
            $data = [];
            foreach ($orders as $num => $item)
            {
                foreach ($item->getOrderItems as $no => $orderItem){
                    $data[] = [
                        'order_item_id' => $orderItem->id,
                        'climbing_tool_name' => $orderItem->getClimbingTool->climbing_tool_name,
                        'quantity' => $orderItem->order_item_quantity,
                        'price' => $orderItem->getClimbingTool->climbing_tool_price,
                        'picture' => 'public/uploads/climbing-tools/'.$orderItem->getClimbingTool->climbing_tool_categories_id.'/'.$orderItem->getClimbingTool->climbing_tool_picture,
                    ];
                }
            }

            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Success add quantity product in cart!',
                'data' => $data
            ];


            return response()->json($params);

        }catch (\Exception $e){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 500, 'Failed to add item quantity!', json_encode($requestParams) );
        }

    }

    public function reduceQuantity(Request $request)
    {
        $apiName = "API_REDUCE_QUANTITY";
        $userId = $request->input('user_id');
        $orderItemId = $request->input('order_item_id');

        $requestParams = [
            'user_id' => $userId,
            'order_item_id' => $orderItemId
        ];

        if(is_null($userId)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Missing required paramter user_id', json_encode($requestParams) );
        }

        if(is_null($orderItemId)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Missing required paramter order_item_id', json_encode($requestParams) );
        }

        $activeUser = User::find($userId);
        if(is_null($activeUser)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'User not found!', json_encode($requestParams) );
        }

        $orderItem = OrderItem::find($orderItemId);
        if(is_null($orderItem)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'Order item not found!', json_encode($requestParams) );
        }

        $climbingToolItem = ClimbingTool::find($orderItem->order_item_climbing_tools_id);
        if(is_null($climbingToolItem)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'Climbing tool item not found!', json_encode($requestParams) );
        }


        if( ($climbingToolItem->climbing_tool_stock - 1) < 0 ){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Climbing tool item out of stock!', json_encode($requestParams) );
        }

        try{
            $orderItem->order_item_quantity -= 1;
            $orderItem->save();

            if($orderItem->order_item_quantity < 1){
                $orderItem->delete();
            }

            $orders = Order::where(['order_users_id' => $userId, 'order_order_status_id' => 1])->get();
            $data = [];
            foreach ($orders as $num => $item)
            {
                foreach ($item->getOrderItems as $no => $orderItem){
                    $data[] = [
                        'order_item_id' => $orderItem->id,
                        'climbing_tool_name' => $orderItem->getClimbingTool->climbing_tool_name,
                        'quantity' => $orderItem->order_item_quantity,
                        'price' => $orderItem->getClimbingTool->climbing_tool_price,
                        'picture' => 'public/uploads/climbing-tools/'.$orderItem->getClimbingTool->climbing_tool_categories_id.'/'.$orderItem->getClimbingTool->climbing_tool_picture,
                    ];
                }

            }

            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Success reduce quantity product in cart!',
                'data' => $data
            ];

            return response()->json($params);

        }catch (\Exception $e){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 500, 'Failed to add item quantity!', json_encode($requestParams) );
        }

    }


    public function normalizationCart($user_id)
    {
        $apiName = "API_NORMALIZE_CART";
        $requestParams = [
            'user_id' => $user_id
        ];
        if(is_null($user_id)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Missing required paramter user_id', json_encode($requestParams) );
        }

        $activeUser = User::find($user_id);
        if(is_null($activeUser)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, 'User not found!', json_encode($requestParams) );
        }

        $orders = Order::where(['order_users_id' => $user_id, 'order_order_status_id' => 1])->get();
        $data = [];
        foreach ($orders as $num => $order) {
            foreach ($order->getOrderItems as $no => $orderItem){
                $climbingToolItem = ClimbingTool::find($orderItem->order_item_climbing_tools_id);
                if($orderItem->order_item_quantity > $climbingToolItem->climbing_tool_stock)
                {
                    $orderItem->order_item_quantity = $climbingToolItem->climbing_tool_stock;
                    try{
                        $orderItem->save();
                    }catch (\Exception $e){
                        return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 400, 'Failed to normalize cart!', json_encode($requestParams) );
                    }
                }

                $data[] = [
                    'order_item_id' => $orderItem->id,
                    'climbing_tool_name' => $orderItem->getClimbingTool->climbing_tool_name,
                    'quantity' => $orderItem->order_item_quantity,
                    'price' => $orderItem->getClimbingTool->climbing_tool_price,
                    'picture' => 'public/uploads/climbing-tools/'.$orderItem->getClimbingTool->climbing_tool_categories_id.'/'.$orderItem->getClimbingTool->climbing_tool_picture,
                ];

            }
        }

        $params = [
            'code' => 302,
            'description' => 'Found',
            'message' => 'Success normalize product in cart!',
            'data' => $data
        ];


        return response()->json($params);

    }



}