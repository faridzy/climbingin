<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.51
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Classes\MessageSystemFunctionalClass;
use App\Models\User;
use Illuminate\Http\Request;

class ApiRegisterController extends Controller
{
    private $messageSystemFunctionalClass;

    public  function __construct(){

        $this->messageSystemFunctionalClass = new MessageSystemFunctionalClass();

    }

    public  function  register(Request $request){
        $apiName = "REGISTER";
        $username = $request->username;
        $password = $request->password;
        $identityNumber = $request->identity_number;
        $address = $request->address;
        $phoneNumber = $request->phone_number;
        $name = $request->name;
        $email = $request->email;
        $postCode = $request->post_code;
        $birthDate=$request->birth_date;
        $gender=$request->gender;

        $sendingParams = [
            'username' => $username,
            'password' => $password,
            'identity_number' => $identityNumber,
            'address' => $address,
            'phone_number' => $phoneNumber,
            'name' => $name,
            'email' => $email,
            'postcode' => $postCode,
            'birth_date'=>$birthDate,
            'gender'=>$gender
        ];

        if(is_null($username)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter username!", json_encode($sendingParams) );
        }

        if(is_null($password)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter password!", json_encode($sendingParams) );
        }


        if(is_null($identityNumber)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter identity_number!", json_encode($sendingParams) );
        }


        if(is_null($address)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter address!", json_encode($sendingParams) );
        }


        if(is_null($phoneNumber)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter phone_number!", json_encode($sendingParams) );
        }


        if(is_null($name)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter name!", json_encode($sendingParams) );
        }


        if(is_null($email)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter email!", json_encode($sendingParams) );
        }

        if(is_null($postCode))
        {
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter postcode!", json_encode($sendingParams) );
        }

        if(is_null($birthDate))
        {
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter birtdate!", json_encode($sendingParams) );
        }

        if(is_null($gender))
        {
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 404, "Missing required parameter gender!", json_encode($sendingParams) );
        }

        $activeUser = User::where(['user_username' => $username,'user_role'=>2])->first();
        if(!is_null($activeUser)){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 403, "Username already used!", json_encode($sendingParams) );
        }

        $data = new User();
        $data->user_username = $username;
        $data->user_password = sha1($password);
        $data->user_identity_number = $identityNumber;
        $data->user_address = $address;
        $data->user_cities_id = 1;
        $data->user_phone_number = $phoneNumber;
        $data->user_fullname = $name;
        $data->user_email = $email;
        $data->user_post_code = $postCode;
        $data->user_birthdate =$birthDate;
        $data->user_gender=$gender;
        $data->user_role=2;


        try{
            $data->save();
            $params = [
                'code' => 302,
                'description' => 'Found',
                'message' => 'Registration success!',
            ];

            return response()->json($params);
        }catch (\Exception $e){
            return $this->messageSystemFunctionalClass->returnApiMessage($apiName, 403, "Registration failed!", json_encode($sendingParams) );
        }

    }



}