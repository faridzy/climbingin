<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.56
 */

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\ClimbingToolCategory;
use Illuminate\Http\Request;

class ClimbingToolCategoryController extends Controller
{
    public  function index()
    {
        $data=ClimbingToolCategory::all();
        $params=[
            'title'=>'Manajemen Kategori Alat Pendaki',
            'data'=>$data
        ];

        return view('backend.master.climbing-tool-categories.index',$params);

    }

    public function form(Request $request){
        $id=$request->id;
        if($id){
            $data=ClimbingToolCategory::find($id);
        }else{
            $data=new ClimbingToolCategory();
        }

        $params=[
            'title'=>'Manajemen Kategori Alat Pendaki',
            'data'=>$data
        ];

        return view('backend.master.climbing-tool-categories.form',$params);

    }

    public function save(Request $request){
        $id=$request->id;
        if($id){
            $data=ClimbingToolCategory::find($id);

        }else{
            $data=new ClimbingToolCategory();
            $checkData=ClimbingToolCategory::where(['climbing_tool_category_name'=>$data->climbing_tool_category_name])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
            }
        }
        $data->climbing_tool_category_name=$request->climbing_tool_category_name;
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil disimpan!</div>
            <script> scrollToTop(); reload(1000); </script>";

        }catch (\Exception $e){
            return "<div class='alert alert-danger'>Data gagal disimpan!</div>";

        }

    }

    public function delete(Request $request){
        $id = $request->id;

        try{
            ClimbingToolCategory::find($id)->delete();
            return "
            <div class='alert alert-success'>Data berhasil dihapus!</div>
            <script> scrollToTop(); reload(1000); </script>";
        }catch (\Exception $e){
            return "<div class='alert alert-danger'>Data gagal dihapus!</div>";
        }
    }



}