<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.55
 */

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public function index(){

        $data=User::all();
        $params=[
            'title'=>'Manejemen Pengguna',
            'data'=>$data
        ];

        return view('backend.master.users.index',$params);

    }

    public function form(Request $request){
        $id = $request->input('id');
        if($id){
            $data = User::find($id);
        }else{
            $data = new User();
        }

        $params = [
            'data' => $data
        ];

        return view('backend.master.users.form', $params);
    }

    public  function save(Request $request){
        $id = $request->id;
        if($id){
            $data = User::find($id);
        }else{
            $data = new User();
            $data->user_cities_id = 1;
        }

        $data->user_username = $request->user_username;
        $data->user_password = sha1($request->user_password);
        $data->user_identity_number = $request->user_identity_number;
        $data->user_address = $request->user_address;
        $data->user_post_code = $request->user_post_code;
        $data->user_phone_number = $request->user_phone_number;
        $data->user_fullname = $request->user_fullname;
        $data->user_email = $request->user_email;
        $data->user_role=2;
        $data->user_gender=$request->user_gender;
        $data->user_birthdate=$request->user_birthdate;

        $picture = $request->file('user_picture');
        if (isset($picture)) {
            $destinationPath='public/uploads/users';
            if (!file_exists($destinationPath)) {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            $filename = date("YmdHis"). '-' . $picture->getClientOriginalName();
            if ($picture->move($destinationPath, $filename)) {
                $data->user_picture = $filename;
            }
        }

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil disimpan!</div>
            <script> scrollToTop(); reload(1000); </script>";
        }catch (\Exception $e){
            return "<div class='alert alert-danger'>Data gagal disimpan!</div>";
        }

    }

    public  function delete(Request $request){

        $id = $request->id;
        $data = User::find($id);
        try{
            $data->delete();
            return "
            <div class='alert alert-success'>Data berhasil dihapus!</div>
            <script> scrollToTop(); reload(1000); </script>";
        }catch (\Exception $e){
            return "<div class='alert alert-danger'>Data gagal dihapus!</div>";
        }

    }

}