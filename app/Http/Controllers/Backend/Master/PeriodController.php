<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.56
 */

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\Period;
use Illuminate\Http\Request;
class PeriodController extends Controller
{
    public  function index()
    {
        $data=Period::all();
        $params=[
            'title'=>'Manajemen Periode',
            'data'=>$data
        ];

        return view('backend.master.periods.index',$params);

    }

    public function form(Request $request){
        $id=$request->id;
        if($id){
            $data=Period::find($id);
        }else{
            $data=new Period();
        }

        $params=[
            'title'=>'Manajemen Periode',
            'data'=>$data
        ];

        return view('backend.master.periods.form',$params);

    }

    public function save(Request $request){
        $id=$request->id;
        if($id){
            $data=Period::find($id);

        }else{
            $data=new Period();
        }
        $data->period_name=$request->period_name;
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil disimpan!</div>
            <script> scrollToTop(); reload(1000); </script>";

        }catch (\Exception $e){
            return "<div class='alert alert-danger'>Data gagal disimpan!</div>";

        }

    }

    public function delete(Request $request){
        $id = $request->id;

        try{
            Period::find($id)->delete();
            return "
            <div class='alert alert-success'>Data berhasil dihapus!</div>
            <script> scrollToTop(); reload(1000); </script>";
        }catch (\Exception $e){
            return "<div class='alert alert-danger'>Data gagal dihapus!</div>";
        }
    }

}