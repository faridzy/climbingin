<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.55
 */

namespace App\Http\Controllers\Backend\Master;


use App\Http\Controllers\Controller;
use App\Models\ClimbingTool;
use App\Models\ClimbingToolCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class ClimbingToolController extends Controller
{
    public function index(){
        $data=ClimbingTool::all();
        $params=[
            'title'=>'Manajemen Alat Pendaki',
            'data'=>$data
        ];

        return view('backend.master.climbing-tools.index',$params);

    }
    public  function form(Request $request){
        $id=$request->id;
        if($id){
            $data=ClimbingTool::find($id);
        }else{
            $data=new ClimbingTool();
        }

        $params=[
            'title'=>'Manajemen Alat Pendaki',
            'data'=>$data,
            'categoryOption'=>ClimbingToolCategory::all()
        ];

        return view('backend.master.climbing-tools.form',$params);


    }
    public function save(Request $request){
        $id=$request->id;
        if($id){
            $data=ClimbingTool::find($id);
        }else{
            $data=new ClimbingTool();
            $checkData=ClimbingTool::where(['climbing_tool_name'=>$data->climbing_tool_name])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
            }
        }

        if(is_null($data->climbing_tool_picture)){
            if(is_null($request->file('climbing_tool_picture')) && $request->file('climbing_tool_picture')==''){
                $fileClimbingToolPicture=$data->climbing_tool_picture;
            }else{
                $rules=[
                    'climbing_tool_picture' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";

                }else{
                    $climbingToolPicture=$request->file('climbing_tool_picture');
                    $destinationPath = 'public/uploads/climbing-tools/'.$request->climbing_tool_categories_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileClimbingToolPicture = date('YmdHis') . '_' . $climbingToolPicture->getClientOriginalName();
                    $climbingToolPicture->move($destinationPath, $fileClimbingToolPicture);
                }

            }

        }

        if(!is_null($data->climbing_tool_picture)){
            if(is_null($request->file('climbing_tool_picture')) && $request->file('climbing_tool_picture')==''){
                $fileClimbingToolPicture=$data->climbing_tool_picture;
            }else{
                $rules=[
                    'climbing_tool_picture' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";

                }else{
                    $climbingToolPicture=$request->file('climbing_tool_picture');
                    $destinationPath = 'public/uploads/climbing-tools/'.$request->climbing_tool_categories_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileClimbingToolPicture = date('YmdHis') . '_' . $climbingToolPicture->getClientOriginalName();
                    $climbingToolPicture->move($destinationPath, $fileClimbingToolPicture);
                }

            }
        }

        $data->climbing_tool_name=$request->climbing_tool_name;
        $data->climbing_tool_description=$request->climbing_tool_description;
        $data->climbing_tool_price=$request->climbing_tool_price;
        $data->climbing_tool_stock=$request->climbing_tool_stock;
        $data->climbing_tool_picture=$fileClimbingToolPicture;
        $data->climbing_tool_categories_id=$request->climbing_tool_categories_id;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Data berhasil disimpan!</div>
            <script> scrollToTop(); reload(1000); </script>";

        }catch (\Exception $e){
            return "<div class='alert alert-danger'>Data gagal disimpan!</div>";

        }


    }

    public function delete(Request $request){
        $id = $request->id;

        try{
            $data=ClimbingTool::find($id);
            $destinationPath = 'public/uploads/climbing-tools/'.$data->climbing_tool_categories_id;
            unlink($destinationPath.'/'.$data->climbing_tool_picture);
            $data->delete();
            return "
            <div class='alert alert-success'>Data berhasil dihapus!</div>
            <script> scrollToTop(); reload(1000); </script>";
        }catch (\Exception $e){
            return "<div class='alert alert-danger'>Data gagal dihapus!</div>";
        }

    }
    public function updateStock(){

    }

}