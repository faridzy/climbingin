<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.57
 */

namespace App\Http\Controllers\Backend\Transaction;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RecordTransactionController extends Controller
{
    public function index()
    {
        return view('backend.transaction.record.index');
    }

    public function show(Request $request)
    {
        $orderStatus = $request->status;
        $startDate = date("Y/m/d", strtotime($request->start_date));
        $endDate = date("Y/m/d", strtotime($request->end_date));

        if($orderStatus == 0){
            $data = Order::whereRaw("order_created_at BETWEEN  '".date("Y-m-d H:i:s", strtotime($startDate))."' AND '".date("Y-m-d H:i:s", strtotime($endDate))."' ")
                ->orderBy('order_created_at', 'DESC')
                ->get();
        }else{
            $data = Order::where(['order_order_status_id' => $orderStatus])
                ->whereRaw("order_created_at BETWEEN  '".date("Y-m-d H:i:s", strtotime($startDate))."' AND '".date("Y-m-d H:i:s", strtotime($endDate))."' ")
                ->orderBy('order_created_at', 'DESC')
                ->get();
        }

        $params = [
            'data' => $data
        ];

        return view('backend.transaction.record.result', $params);
    }

    public function detail(Request $request)
    {
        $id = $request->id;
        if($id){

            $order = Order::find($id);
            if(!is_null($order)){
                $orderItems = $order->getOrderItems;

                $params = [
                    'order' => $order,
                    'orderItems' => $orderItems,

                ];

                return view('backend.transaction.record.detail', $params);

            }else{
                return "<div class='alert alert-danger'>Order tidak tersedia!</div>";
            }

        }else{
            return "<div class='alert alert-danger'>Data tidak tersedia!</div>";
        }
    }

}