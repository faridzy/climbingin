<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.56
 */

namespace App\Http\Controllers\Backend\Transaction;


use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\OrderStatus;

class InTransactionController extends Controller
{
    public function index()
    {
        $data = Order::whereIn('order_order_status_id' ,[1,2,3])
            ->orderBy('order_created_at', 'DESC')->get();

        $params = [
            'data' => $data
        ];
        return view('backend.transaction.in.index', $params);
    }

    public function detail(Request $request)
    {
        $id = $request->id;
        if($id){

            $order = Order::find($id);
            if(!is_null($order)){
                $orderItems = $order->getOrderItems;

                $params = [
                    'order' => $order,
                    'orderItems' => $orderItems

                ];

                return view('backend.transaction.in.detail', $params);

            }else{
                return "<div class='alert alert-danger'>Order tidak tersedia!</div>";
            }

        }else{
            return "<div class='alert alert-danger'>Data tidak tersedia!</div>";
        }
    }

    public function updateToProcess(Request $request)
    {
        $id = $request->id;
        if(is_null($id)){
            return "<div class='alert alert-danger'>Order tidak tersedia!</div>";
        }else{
            $data = Order::find($id);
            $data->order_order_status_id = 2;
            try{
                $data->save();

                return "
                <div class='alert alert-success'>Data berhasil disimpan!</div>
                <script> scrollToTop(); reload(1000); </script>";
            }catch (\Exception $e){
                return "<div class='alert alert-danger'>Data gagal disimpan!</div>";
            }
        }
    }

}