<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.55
 */

namespace App\Http\Controllers\Backend\Feedback;


use App\Http\Controllers\Controller;
use App\Models\Order;

class FeedbackController extends Controller
{
    public function index()
    {
        $data = Order::whereIn('order_order_status_id',[1,2,3,4])
            ->whereNotNull('order_user_feedback')
            ->orderBy('order_created_at', 'DESC')->get();
        $params = [
            'data' => $data
        ];

        return view('backend.feedback.record.index', $params);
    }

}