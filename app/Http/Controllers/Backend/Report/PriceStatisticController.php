<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.59
 */

namespace App\Http\Controllers\Backend\Report;


use App\Http\Controllers\Controller;
use App\Models\ClimbingTool;

class PriceStatisticController extends Controller
{
    public function index()
    {
        $climbingTool = ClimbingTool::all();
        $data = [];
        foreach ($climbingTool as $num => $item){
            $data[] = [
                'name'=>$item->climbing_tool_name,
                'price'=>$item->climbing_tool_price
            ];
        }

        $params = [
            'data' => $data
        ];

        return view('backend.report.price-statistic.index', $params);
    }

}