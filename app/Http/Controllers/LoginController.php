<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 15.08
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        if ($request->session()->exists('activeUser')) {
            return redirect('/backend');
        }
        return view('login.index');
    }

    public  function login(Request $request){
        $username=$request->username;
        $password=sha1($request->password);

        $activeUser=User::where(['user_username'=>$username])->first();
        if(is_null($activeUser)){
            return "<div class='alert alert-danger'>Pengguna tidak terdaftar</div>";
        }else{
            if($activeUser->user_password !=$password){
                return "<div class='alert alert-danger'>Password salah</div>";

            }elseif ($activeUser->user_role!=1){
                return "<div class='alert alert-danger'>Anda tidak dizinkan masuk pada halaman ini!</div>";

            }else{
                $request->session()->put('activeUser', $activeUser);
                return "
            <div class='alert alert-success'>Login berhasil!</div>
            <script> scrollToTop(); reload(1000); </script>";


            }
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');
    }


}