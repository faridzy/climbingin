<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 14.51
 */

namespace App\Classes;


use App\Models\ApiLog;
use App\Models\MessageSystem;

class MessageSystemFunctionalClass
{
    private function  getMessage($code)
    {
        $data = MessageSystem::where(['message_system_code' => $code])->first();
        return $data;
    }

    public function returnApiMessage($apiName,$code,$description,$params)
    {
        $data = new ApiLog();
        $data->api_log_name = $apiName;
        $data->api_log_description = $description;
        $data->api_log_error_code = $code;
        $data->api_log_params = $params;
        $data->save();


        $messageSystem = $this->getMessage($code);
        if(!is_null($messageSystem)){
            $params = [
                'code' => $messageSystem->message_system_code,
                'description' => $messageSystem->message_system_description,
                'message' => $description,
            ];
        }else{
            $params = [
                'code' => 00,
                'description' => "Message system unavailable",
                'message' => $description
            ];
        }

        return response()->json($params);


    }


}