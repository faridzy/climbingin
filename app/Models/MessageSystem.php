<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 14.38
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MessageSystem extends Model
{
    protected $table = 'message_systems';
    protected $primaryKey = 'id';
    public $timestamps = false;

}