<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 14.40
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getOrder()
    {
        return $this->hasOne('App\Models\Order', 'id', 'order_item_orders_id');
    }

    public  function  getClimbingTool()
    {
        return $this->hasOne('App\Models\ClimbingTool','id','order_item_climbing_tools_id');
    }

}