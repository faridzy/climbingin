<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 14.40
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'order_users_id');
    }

    public function getOrderItems()
    {
        return $this->hasMany('App\Models\OrderItem', 'order_item_orders_id', 'id');
    }

    public function getPeriod()
    {
        return $this->hasOne('App\Models\Period', 'id', 'order_periods_id');
    }


    public function getOrderStatus()
    {
        return $this->hasOne('App\Models\OrderStatus', 'id', 'order_order_status_id');
    }

    public function getPaymentType()
    {
        return $this->hasOne('App\Models\PaymentType', 'id', 'order_payment_types_id');
    }

}