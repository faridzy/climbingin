<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 20.13
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ClimbingToolHistoryStock extends Model
{
    protected $table = 'climbing_tool_history_stocks';
    protected $primaryKey = 'id';
    public $timestamps = false;

}