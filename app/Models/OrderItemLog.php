<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 14.41
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderItemLog extends Model
{
    protected $table = 'order_item_logs';
    protected $primaryKey = 'id';
    public $timestamps = false;

}