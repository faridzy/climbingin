<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 14.41
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ClimbingTool extends Model
{
    protected $table = 'climbing_tools';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getCategory()
    {
        return $this->hasOne('App\Models\ClimbingToolCategory','id','climbing_tool_categories_id');

    }

}