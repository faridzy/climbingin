<?php
/**
 * Created by PhpStorm.
 * User: mfarid
 * Date: 20/05/18
 * Time: 14.41
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ClimbingToolCategory extends Model
{
    protected $table = 'climbing_tools_categories';
    protected $primaryKey = 'id';
    public $timestamps = false;

}